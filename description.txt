Realtime Clock is a minimal library for custom formatting of the in-game date and time without the need for cumbersome mathematical calculations. I published the source code on the forums a couple years ago, but I decided it might be more useful as a standalone mod in case of dependencies.

For more information: https://forum.minetest.net/viewtopic.php?f=9&t=23885
